#+TITLE: Running OpenShift in Production
#+AUTHOR: Wilhelm Weber / Toni Schmidbauer
#+REVEAL_HLEVEL: 2
#+REVEAL_THEME: white
#+REVEAL_TRANS: none
#+OPTIONS: toc:nil
#+OPTIONS: num:nil
#+OPTIONS: ^:nil

* Agenda

- Who are we that we dare to talk about OpenShift
- Platform
- OpenShift
- Monitoring
- Pain points
- Improvements
- Recommendations
- Q & A

* whoarewe

** Wilhelm Weber
[[./images/willi.svg]]

** Toni Schmidbauer
[[./images/toni.svg]]

** and

- Two software architects (actually writing code)

* Motivation for using OpenShift

- Because it's in the right top of the gartner hype quadrant
- Big monolithic applications
- Deployments took several hours
- Developer happiness


* Platform
** Platform choice

- VMWare VSphere (6)
- RedHat Enterprise Linux (7) / NOT Atomic
- OpenShift Container Platform (3.3)
- Currently 45 nodes in total

** Platform setup

- RedHat Cloudforms to provision hosts on VSphere
  - Single location nodes
  - OpenShift manages HA
- Puppet for base OS configuration
- Ansible for installation and post-installation

** Puppet manages

- Users and ssh keys (e.g. ansible)
- Packages
- Firewall rules
- Systemd services (e.g. Backup jobs)
- Docker configuration
- LVM volumes
- Docker storage setup
- Sysctl settings (ipforwarding !!!!!)
- Keepalived for OpenShift API
- Fluent installation / configuration
- Ansible inventory

** Puppet ansible inventory management

- Inventory fully populated from puppet configuration data
- New nodes are automatically added to inventory
- Hiera / CouchDB is your friend

** Pitfalls when using puppet

** The problem

- docker cfg is managed by puppet (/etc/sysconfig/docker)
- OSCP upgrade modifies /etc/sysconfig/docker
- Docker gets restarted after upgrades when the puppet agent runs
- Pods may fail when docker restarts

** The solution

- create /etc/systemd/system/docker.service.d/chopsuey.conf
- EnvironmentFile=/etc/sysconfig/docker.chopsuey
- Settings in /etc/sysconfig/docker.chopsuey overwrite settings in /etc/sysconfig/docker
- keep your environment settings in docker.chopsuey
  - http proxy settings
  - block docker hub
  - docker log rotation settings

** so why puppet at all?

- It's our hammer, OpenShift looked like a nail
- Base host configuration is fully managed
- Yes, we also use ansible, it's great

** Ansible manages
- Installation of OpenShift
- Creating router
- Deploys applications we need for our environment
  - Deployment tool (called CS ToolBox)
  - Internal services
- Configures roles and SCC's we need

** Ansible inventory

#+BEGIN_SRC sh
$ find /etc/ansible/inventory
/etc/ansible/inventory/hosts
/etc/ansible/inventory/group_vars/all/chopsuey
/etc/ansible/inventory/master01
/etc/ansible/inventory/node01
$ cat /etc/ansible/inventory/master01
[masters]
cerebrod01.eb.lan.at ansible_become=true

[etcd]
cerebrod01.eb.lan.at

[nodes]
cerebrod01 openshift_schedulable=true openshift_node_labels=...
#+END_SRC

- /hosts/ includes common settings for OSCP
- /group_vars/ for common variables
- on file for every master / node
** Host configuration

** FS layout
- /var/lib/docker
- /var/lib/etcd (in the queue)
- /app (for application logs, scripts ...)
- LVM Thin volumes for Docker (recommended by RedHat)

** On memory

- we started without swap
- OSCP 3.2 recommend enabling swap (which we did)
- OSCP 3.3 recommend disabling swap (which we are doing now)
- Avoid oversubscribing
- disabled transparent huge pages (we are not running databases or
  vm's, but redis)

** reserving resources for nodes

- kube-reserved/system-reserved in node-config.yaml
- we just start to play around with these

** logfile handling
- Fluentd/Elastic/Kibana for system logs
- Fluentd + custom built log file mgmt app for application logs
- Applications log to stdout in the container
- Docker stores output in json files

** Backup / recovery

 - Registry backup (daily)
 - Etcd backup (daily and on upgrade)
 - System backup (daily)

** Satellite
- Before 3.3 running OSCP without RedHat Satellite was a bad idea
- Now there's atomic-openshift-excluder
- CCV Redhat OpenShift
- CV for every product
  - RHEL base
  - RHEL SCL
  - RHEL extras
  - OpenShift

** Satellite cont.

- atomic-openshift-excluder does not exclude etcd in 3.3
- so when running yum update exclude
  - rhel-7-server-ose-3.*-rpms
  - rhel-7-server-extras-rpms
- we got hit by this! etcd got upgrade from 2.3 to 3.1
- newer etcd could not join old 2.3 cluster (surprise, surprise...)

** OS Upgrades

- disable all OpenShift repos
- disable extras repo
- or make sure that
  - atomic-*
  - etcd-*
  - docker-*
  are excluded

* OpenShift
** History

- Started with evaluating Kubernetes
- Moved to OpenShift Origin
- Now on OpenShift Container Platform 3.3

** OpenShift setup

- 3 Datacenters (DC1, DC2, DC3)
- 3 masters
- infrastructure nodes
- worker nodes
- security zones
- LDAP for authentication
- Labels are your friends

** OpenShift setup

[[./images/cluster_setup.svg]]

** Labels we use

- DC1/DC2/DC3
- Node role (worker, infrastructure)
- Labels for custom software
- Labels for our security zones

** Persistent Volumes

- NFS
- Ceph RBD
- (GlusterFS)

** Certificates

- Node / Master communication uses SSL
- Certificates managed by OpenShift (internal CA)
- ETCD communictation also uses SSL, but a different CA
- Playbook for replacing certificates, it just works

** Master API accessibility

 - Do not use the external HAProxy, as it's a SPOF
 - We currently use /keepalived/ on masters (not within docker)
 - Problem: all requests hit one master

** Upgrading

- disable scheduling and evacutate nodes
- Lock ansible inventory so puppet won't touch it
  #+BEGIN_SRC sh
  echo 'openshift_inventory_locked=true' > /etc/facter/facts.d/openshift.txt
  #+END_SRC

- Remove 1/2 of nodes
  #+BEGIN_SRC sh
  rm /etc/ansible/inventory/<host files>
  #+END_SRC
- Upgrade masters and nodes still in inventory
- Test
- Unlock inventory
  #+BEGIN_SRC sh
  $ rm /etc/facter/facts.d/openshift.txt
  $ puppet agent -t
  #+END_SRC

** Upgrading cont.

- upgrade remaining nodes
- Nowadays you can use /openshift_upgrade_node_label/

** Regarding SCC's (Security Context Constraints)

 - Use your own
 - Do not modify the built in ones
 - They get overwritten on upgrade
 - Yes it's documented, no we didn't read that part

** Deploying Applications

 - Source code and build artifacts in separate repositories
 - Jenkins (deployed in OpenShift) builds app (pipeline)
 - Pipeline step checks war/jar into Gitlab repository (application repo)
   - Developers are more used to Jenkins pipelines
   - We do not want to store build tools in our production containers

** Deploying Applications cont.
 - OpenShift builder pod uses application repo for building image
 - Developer deploy's image

** Deployments

- Chopsuey ToolBox (custom)
- <project>-<application name>-origin repo
  - DC
  - route
  - service
- <project>-<application name>-application
  - contains build artifact
- <project>-project
  - quota
  - limits
* Monitoring

** Icinga
  - ETCD running
  - HAProxy running
  - Master API accessible
  - Docker registry FS usage
  - Thin volume
    (DO THIS, or hell will break loose)
  - Docker FD usage
  - Applications, if they provide a health check URI
    - Discovered via a custom service
  - Building the icinga configuration is automated

** Prometheus

[[./images/prometheus_setup.svg]]

** Grafana

[[./images/prometheus_screen.png]]

* Pain points

- Started with too few nodes
- Image management
  - updates
  - responsibility
- Security zones (we are dumping them)
- Docker sucks, kind of
  e.g. leaks file descriptors, API breakage
- We currently have to rebuild images for every cluster

* Pain points cont.

- Firewall rules
  - PODs can run everywhere
  - Allow the whole cluster
  - Or use a egress router
- Management of PV/PVC has to be automated
- OpenShift API is only accessible via one ip (keepalive)
  - We need to configure a loadbalancer in front of the API

* Improvements
- Chaos Monkey
- Use OpenShift dynamic storage provisioning (3.4)
  - Heketi (https://github.com/heketi/heketi)
  - Trident (https://github.com/NetApp/trident)
- Use Gitlab docker registry to move images between clusters
* Improvements cont.
- Alertmanager -> Icinga integration
- VSphere performance data via /vsphere_exporter/
- Use dedicated ESX hosts with internal disks
- LB for the OpenShift API
- LB for router, DNS round robin only works for some resolvers
* Recommendations

- Study the reference architecture
  - https://blog.openshift.com/openshift-container-platform-reference-architecture-implementation-guides/
- Do not save on nodes
- You need infrastructure nodes
- Monitor IOPS of your ETCD (masters in our case)
- Really read the release notes / upgrade guide _before_ upgrading

* Community

- openshift-dev mailing list
- openshift-users mailing list
- http://www.openshift-anwender.de
- https://openshift-de.slack.com
- #openshift-dev channel on freenode
- http://stackoverflow.com/questions/tagged/openshift-origin

* Link

https://gitlab.com/nosolutions/linuxwochen/openshift/
